function [t,x,y,z] = myLorenz(sigma,beta,rho,IC,Fs,tend);
%tend = 100;           %Total Simulation Time in seconds
%Fs = 44100/100;%/1000;  %Sampling rate
%Parameters
%sigma=10;           %Pr = Momentum Diffusivity/Thermal Diffusivity
%beta=(8.0/3.0);     %Geometrical Aspect ratio
%rho= 28             %sigma*(sigma+beta+3)/(sigma-beta-1);
%Initial Conditions
%IC = []
t(1)=0;  
x(1)= IC(1);%sqrt(beta*(rho-1))+0.0001;
y(1)= IC(2);%sqrt(beta*(rho-1))+0.0001;
z(1)= IC(3);%rho -1+0.001;
dt=(1/Fs);
t=0:dt:tend;
f=@(t,x,y,z) sigma*(y-x);  
g=@(t,x,y,z) x*rho-x.*z-y;
p=@(t,x,y,z) x.*y-beta*z;
for i=1:(length(t)-1) 
    k1=f(t(i),x(i),y(i),z(i));
    l1=g(t(i),x(i),y(i),z(i));
    m1=p(t(i),x(i),y(i),z(i));
      k2=f(t(i)+dt/2,(x(i)+0.5*k1*dt),(y(i)+(0.5*l1*dt)),(z(i)+(0.5*m1*dt)));     
      l2=g(t(i)+dt/2,(x(i)+0.5*k1*dt),(y(i)+(0.5*l1*dt)),(z(i)+(0.5*m1*dt)));
      m2=p(t(i)+dt/2,(x(i)+0.5*k1*dt),(y(i)+(0.5*l1*dt)),(z(i)+(0.5*m1*dt)));
      k3=f(t(i)+dt/2,(x(i)+0.5*k2*dt),(y(i)+(0.5*l2*dt)),(z(i)+(0.5*m2*dt)));
      l3=g(t(i)+dt/2,(x(i)+0.5*k2*dt),(y(i)+(0.5*l2*dt)),(z(i)+(0.5*m2*dt)));
      m3=p(t(i)+dt/2,(x(i)+0.5*k2*dt),(y(i)+(0.5*l2*dt)),(z(i)+(0.5*m2*dt)));
      k4=f(t(i)+dt,(x(i)+k3*dt),(y(i)+l3*dt),(z(i)+m3*dt));
      l4=g(t(i)+dt,(x(i)+k3*dt),(y(i)+l3*dt),(z(i)+m3*dt));
      m4=p(t(i)+dt,(x(i)+k3*dt),(y(i)+l3*dt),(z(i)+m3*dt));
      x(i+1) = x(i) + dt*(k1 + 2*k2 +2*k3 +k4)/6; 
      y(i+1) = y(i) + dt*(l1 + 2*l2 + 2*l3 +l4)/6;
      z(i+1) = z(i) + dt*(m1 + 2*m2 + 2*m3 +m4)/6;    
end

% get(0,'Factory');
% set(0,'defaultfigurecolor',[1 1 1]);
% figure
% hold on;
% view(3)
% grid on;
% plot3(x,y,z)
% plot3(-sqrt(beta*(rho-1)),-sqrt(beta*(rho-1)),rho -1,'kx');
% plot3(sqrt(beta*(rho-1)),sqrt(beta*(rho-1)),rho -1,'kh');
% plot3(0,0,0,'k*')
% legend(['IC: (',num2str(x(1)),',',...
%         num2str(y(1)),',', num2str(z(1)),')'],['fixed point C-'],...
%         ['fixed point C+'],[ 'Origin (0,0,0)']);
% xlabel('x(t)'); ylabel('y(t)'); zlabel('z(t)');
% axis([min(x1) max(x1) min(x2) max(x2) min(x3) max(x3)])
% title(['Lorenz Phase Space: sigma=',num2str(sigma),...
%         ' beta=',num2str(beta),' r=',num2str(rho),' tend=', num2str(tend)...
%         ,' dt=',num2str(dt)])   
end
