## The Lorenz Attractor
This repository includes the numerical solution via the Runge-Kutta 4th order implementation of the Lorenz System. It also includes MATLAB animations that aim to visualize some important phenomena. Some brief documentation is included.

![Alt Text](/Figures/ChaoticTrajectories2.gif)

1. Blue Line started at Initial Conditions (x0,y0,z0) = (4.2426 2.2426 13.5)
2. Red line started at the same Initial conditions for x0 and z0. BUT y0 was 2.2426+0.001. This small perturbation on only one coordinate led to impossible prediction after a few seconds.


**The Lorenz System** is a set of three-dimensional, deterministic, nonlinear autonomous equations. 
It was the first system to prove that chaos can originate from purely deterministic laws (no stochasticity) making the theoretically predictable system to be unpredictable for long predictions. 
The equations are autonomous meaning that terms don't include the independent variable (time) explicitly.

![Image alt ><](/Figures/Equations.PNG)

The system has two quadratic nonlinearities xy and xz and it is dependent on three positive parameters which are assumed positive.
The three state variabels x,y and z define the system's state space.
For parameters (sigma = 10, beta = 8/3, rho = 28) the equivalent time series for each state variable when plotted against each other one can see the trajectory of a unique solution for an exact Initial Condition.
In the first animation you can see how two slightly different initial conditions lead to completely different trajectories after a few seconds. 
The trajectories flow around two fixed points and never repeat the same pattern.

**Reconstructed Delay Phase Space** 
As expected the observed dynamics by projecting its signal with it's past shows the attractor for x and y. The two different Trajectories have also a different reconstruction.
As the sampling rate for the finite scheme used was 10,000 Hz. It can be seen that the signal has to be projected with at least 100 lag in order to unfold the attractor.
![Alt Text](/Figures/LorenzRDPS.gif)
