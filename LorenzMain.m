%%% Lorenz Main %%%
clear all
clc
close all
get(0,'Factory');
set(0,'defaultfigurecolor',[1 1 1]);
myPath = pwd
addpath(genpath(pwd))

clear all
clc
get(0,'Factory');
set(0,'defaultfigurecolor',[1 1 1]);

sigma=10;       		%Pr = Momentum Diffusivity/Thermal Diffusivity
beta=(8.0/3.0); 		%Geometrical Aspect ratio
rho=28;
IC1 = [0 0.1 0];
IC1 = [sqrt(beta*(rho-1))/2 sqrt(beta*(rho-1))/2 (rho -1)/2]
dt = 0.0001;
fs = 1/dt;			%44100/100;
tend = 400;
[t,x,y,z] = myLorenz(sigma,beta,rho,IC1,fs,tend);

% a = 500000;
% plot3(x(70000:a),y(70000:a),z(70000:a))
h = 0.001;
IC2 = IC1;
IC2(2) = IC1(2)+h
[t,x1,y1,z1] = myLorenz(sigma,beta,rho,IC2,fs,tend);
% hold on;
% plot3(x1(70000:a),y1(70000:a),z1(70000:a))

A = [x' y' z'];
B = [x1' y1' z1'];
[nrows ncols] = size(A);
Strings = {'Lorenz x','Lorenz y','Lorenz z'};
Markers = {'+','o','*','x','v','d','^','s','>','<'};
Colors = {'b','b','b'} %,'m','g'};
subplotTimeSeries(A,Colors,Strings);

%%%%Trajectory Save Images for Gif Simulation
clear figure
k = round(linspace(70000,500000,300));
%k = round(linspace(70000,500000,300));
figure(1) = figure('units','normalized','outerposition',[0 0 1 1]);

axis([-50 50 -50 50 0 50])

xlabel('x(t)'); ylabel('y(t)'); zlabel('z(t)');
for i = 1:length(k)
    
    hold on;
    view(40,16)
        title([' Lorenz System: 2 Close Trajectories  [ sigma=',num2str(sigma),' beta=',num2str(beta)...
        ,' r =',num2str(rho),', t = ' num2str(t(k(i))-t(k(1))),' ]'])
    if i == 1
        plot3(x(k(1):k(i)),y(k(1):k(i)),z(k(1):k(i)),'b'); hold on;
        plot3(x(k(i)),y(k(i)),z(k(i)),'ob');
        plot3(x1(k(1):k(i)),y1(k(1):k(i)),z1(k(1):k(i)),'r'); hold on;
        plot3(x1(k(i)),y1(k(i)),z1(k(i)),'or');
    else
        plot3(x(k(i-1):k(i)),y(k(i-1):k(i)),z(k(i-1):k(i)),'b'); hold on;
        plot3(x(k(i)),y(k(i)),z(k(i)),'ob');
        plot3(x1(k(i-1):k(i)),y1(k(i-1):k(i)),z1(k(i-1):k(i)),'r'); hold on;
        plot3(x1(k(i)),y1(k(i)),z1(k(i)),'or');  
    end
    filepath = strcat(pwd,'\Lorenz\Figures\')
    filename = strcat(filepath,'Lorenz2Trajectories',num2str(i),'.png');
    saveas(figure(1),filename);
    delete(findobj('marker', 'o'));
end

%%%%Reconstructed delay phase space 
A = [x' y' z'];
B = [x1' y1' z1'];
a = 70000; b = 500000;
A = A(a:b,:);
B = B(a:b,:);
[nrows ncols] = size(A);
clear figure
handle = axes('Parent', figure(1));
figure(1) = figure('units','normalized','outerposition',[0 0 1 1]);
hold(handle, 'off');


tau = round(1:100:length(A));
for i = 1:length(tau); % N-1 %round(N/2)
%         plot3(x(2*tau+1:end),x(tau+1:end-tau),x(1:end-2*tau))
%         title(['Delay Reconstruction: tau = ',num2str(tau)]);
%         xlabel('x(t)'); ylabel('x(t-tau)'); zlabel('x(t-2tau)');
     
    for j = 1:ncols
         subplot(2,ncols,j)
         plot(A(1:end-tau(i),j),A(tau(i)+1:end,j),'color',Colors{j})
         title([Strings{j},' tau = ',num2str(tau(i))]);
         
         subplot(2,ncols,j+ncols)
         plot(B(1:end-tau(i),j),B(tau(i)+1:end,j),'r')
         title([Strings{j},' tau = ',num2str(tau(i))]);
     end
     sgtitle(['Reconstructed Delay Phase Space: N = ',num2str(length(A))])
     drawnow
     
     %tic;
%      filepath = strcat(pwd,'\DelayPhaseSpace\Figures\')
%      filename = strcat(filepath,'RDPS',num2str(tau),'.png');
%      saveas(figure(1),filename);
    filepath = strcat(pwd,'\Lorenz\Figures\')
    filename = strcat(filepath,'LorenzReconstructedDelay',num2str(i),'.png');
    saveas(figure(1),filename);
     %toc;
     pause(0.1)

end