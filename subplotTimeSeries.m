function [] = subplotTimeSeries(x,Colors,Strings)
%Subplot of Time Series
[nrows ncols] = size(x);
%clear figure
%figure(1)
%figure(1) = figure('units','normalized','outerposition',[0 0 1 1]);
for i = 1:ncols
    subplot(ncols,1,i)   
    plot(0:nrows-1,x(:,i),'color',Colors{i})
    t(i) = title([Strings(i),' Time Series']);
    xlabel('time a.u.')
    ylabel('x')    
end
% if fig == 'true'
%     filepath = strcat(pwd,'\DelayPhaseSpace\Figures\')
%     filename = strcat(filepath,'Timeseries1.png');
%     saveas(figure(1),filename);